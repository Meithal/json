#include "json.h"

static inline size_t
in(const char* restrict hay, unsigned char needle) {
    if (needle == '\0') return 0;
    const char * begin = hay;
    for (;*hay; hay++) {
        if(*hay == needle) return (hay - begin) + 1;
    }
    return 0;
}

static inline size_t
len_whitespace(unsigned const char * restrict string) {
    int count = 0;
    while(in(whitespaces, string[count])) {
        count++;
    }
    return count;
}

static inline size_t
remaining(unsigned const char * restrict max, unsigned const char * restrict where) {
    return max - where;
}

EXPORT(void
start_string)(unsigned int * restrict cursor,
             unsigned char pool[STRING_POOL_SIZE]) {
    *cursor += (string_size_type_raw)(pool[*cursor]) + sizeof string_size_type;
    cs_memset(&pool[*cursor], 0, sizeof string_size_type);
}

EXPORT(void
push_string)(const unsigned int *cursor,
            unsigned char *pool,
            unsigned char* string,
            size_t length) {
    /* todo: memmove if we insert */
    string_size_type_raw* sh = (string_size_type_raw*)(void*)&pool[*cursor];
    cs_memcpy(
            pool + *cursor + sizeof string_size_type + *sh,
            string,
            length);
    *sh += (string_size_type_raw)length;
}

EXPORT(void
close_root)(struct token * tokens,
        int * root_index) {
    *root_index = tokens[*root_index].support_index;
}

EXPORT(void
push_root)(int * root_index, const int * token_cursor) {
    *root_index = *token_cursor - 1;
}

static struct token *
tree_root(struct tokens * tokens, struct token * from) {
    while(from->support_index != TOKEN_ROOT &&
        aindex(
                tokens->stack,
                from
        ) != from->support_index
    ) {from = &tokens->stack[from->support_index];}

    return from;
}

EXPORT(void
push_token_kind)(
        enum kind kind,
        void * address,
        struct tokens *tokens,
        int root_index) {

    struct token tok = {
        .kind=kind,
        .support_index=root_index,
        .address=(unsigned char*)address + sizeof string_size_type
    };
    tokens->stack[(tokens->max)++] = tok;
}

EXPORT(void
insert_token_)(struct tree * state, unsigned char *token, struct token* root) {
    int target_root = aindex(state->tokens.stack, root);
    if(target_root != state->current_support) {
        state->tokens.tangled = 1;
    }
    if(token[0] == '>') {
        int i = 0;

        while (token[i] && token[i] == '>') {
            CLOSE_ROOT(state);
            i++;
        }
        return;
    }

    enum kind kind[] = {
            UNSET, JSON_TRUE, JSON_FALSE, JSON_NULL, ARRAY, OBJECT,
            NUMBER, NUMBER, NUMBER, NUMBER, NUMBER, NUMBER,
            NUMBER, NUMBER, NUMBER, NUMBER, NUMBER, STRING};
    enum kind kind2 = kind[in("tfn[{-0123456789\"", token[0])];
    if(kind2 != UNSET) {
        START_AND_INSERT_TOKEN(state, kind2, token, target_root);
    }
    if(in("{[:", token[0])) PUSH_ROOT(state);
    if(kind2 == STRING
        && tok_at(&state->tokens,state->current_support)->kind == OBJECT) {
        PUSH_ROOT(state);
    }
}

EXPORT(void stream_tokens_)(
        struct tree * state, struct token * where,
        char separator, unsigned char *stream, size_t length) {
    size_t i = 0;
    int old_root = state->current_support;
    state->current_support = (int)(where - state->tokens.stack);
    if(old_root != state->current_support) {
        state->tokens.tangled = 1;
    }
    while (i < length) {
        size_t token_length = 0;
        while (i + token_length < length && stream[i + token_length] != separator) {
            token_length++;
        }
        stream[i + token_length] = '\0';
        push_token(state, &stream[i]);
        i = i + token_length + sizeof separator;
    }
    state->current_support = old_root;
}

EXPORT(void start_state)(
        struct tree * state,
        struct token *stack,
        unsigned char *pool
        ) {
    state->cur_state = EXPECT_BOM;
    state->tokens.stack = stack;
    state->strings.pool = pool;
    state->current_support = -1;
    state->tokens.max = 0;
    state->tokens.tangled = 0;
    state->strings.cursor = 0;
}

/**
 * Returns the next sibling from the given root and the given node
 * If the given node is NULL, returns the first child
 * If last child or no child exists, returns NULL.
 */
EXPORT(struct token* next_child_)(
        struct tokens * tokens,
        struct token * root,
        struct token * current) {
    int root_idx = aindex(tokens->stack, root);
    int start = (root_idx + 1) % tokens->max;
    if (current)
        start = (aindex(tokens->stack, current) + 1) % tokens->max;

    if (start <= root_idx && !tokens->tangled) return NULL;

    int i;
    if(tokens->tangled)
        for( /* if tokens are tangled we must test all of them, O(n) */
                i = start;
                i != root_idx;
                i++, i%=tokens->max) {
            if (tokens->stack[i].support_index == root_idx) {
                return &tokens->stack[i];
            }
        }
    else /* if tokens are untangled, break on first sibling with different parent */
        for(i = start; i < tokens->max; i++) /* we still have to browse over nested arrays */
            if (tokens->stack[i].support_index == root_idx) {
                return &tokens->stack[i];
            }
    return NULL;
}

EXPORT(struct token*
query_from)(
        struct tree * state, size_t length,
        unsigned char query[va_(length)],
        struct token * cursor) {
    /* todo : unescape ~0, ~1, ~2 */
    size_t i = 0;

    while (i < length) {
        if (query[i] == '/') {
            if(cursor->kind != OBJECT && cursor->kind != ARRAY) {
                return NULL;
            }
            i++;
            size_t token_length = 0u;
            while (i + token_length < length && query[i + token_length] != '/') {
                token_length++;
            }
            unsigned char buffer[0x80] = { (unsigned char)('"' * (cursor->kind == OBJECT)) };
            cs_memcpy(&buffer[(ptrdiff_t)1 * (cursor->kind == OBJECT)], &query[i], token_length);
            buffer[1 + token_length] = '"' * (cursor->kind == OBJECT);

            if (cursor->kind == ARRAY) {
                int index = 0;
                int j;
                for (j = 0; buffer[j]; j++) {
                    index *= 10;
                    index += buffer[j] - '0';
                }
                struct token * cur = NULL;
                do {
                    cur = next_child(state, cursor, cur);
                } while (index--);
                cursor = cur;
            } else if (cursor->kind == OBJECT) {
                int index = state->tokens.max;
                struct token * cur = NULL;
                do {
                    cur = next_child(state, cursor, cur);
                    if (cs_memcmp(cur->address,
                                  buffer,
                                  token_length + 2) == 0) {
                        if((i + token_length + 1 < length &&
                            query[i+token_length] == '/' && query[i+token_length+1] == '<')) {
                            return cur;
                        }
                            cursor = next_child(
                                    state,
                                    cur,
                                    NULL);

                        break;
                    }
                } while (index--);
            }
            i = i + token_length - 1;
        }
        i++;
    }

    return cursor;
}

EXPORT(struct token*
query_)(struct tree * tree,
        size_t length,
        unsigned char query[va_(length)]) {
    return query_from(
        tree, length, query,
        tree_root(&tree->tokens, &tree->tokens.stack[0])
    );
}

EXPORT(int aindex)(struct token* stack, struct token* which) {
    return (int)(which - stack);
}

EXPORT(void delete_token)(struct token* which) {
    /* todo: this should make the tree tangled but we don't
     * get a reference to it here. */
    which->support_index = -2;
}

EXPORT(void move)(
        struct tree* state, struct token* which, struct token* where) {
    int new_idx = aindex(state->tokens.stack, where);
    if(which->support_index != new_idx) {
        state->tokens.tangled = 1;
    }
    which->support_index = new_idx;
}

EXPORT(void rename_string_)(
        struct tree* state, struct token* which, size_t len, const char* new_name) {
    START_STRING(state, new_name);
    PUSH_STRING(state, "\"", 1);
    PUSH_STRING(state, new_name, len);
    PUSH_STRING(state, "\"", 1);
    which->address = state->strings.pool + state->strings.cursor + sizeof string_size_type;
}

EXPORT(enum json_errors inject_)(size_t len,
       unsigned char text[va_(len)],
       struct tree * state,
       struct token * where) {
    enum states old_state = state->cur_state;
    int old_root = state->current_support;
    state->current_support = (int)(where - (state->tokens.stack));

    if(old_root != 0
    && state->current_support > 1
    && old_root != state->current_support) {
        /* because of the root token we can get false reading here */
        state->tokens.tangled = 1;
    }
    state->cur_state = EXPECT_BOM;
    enum json_errors error = rjson_(
            len, (unsigned char *) text, state);
    state->cur_state = old_state;
    state->current_support = old_root;
    return error;
}

EXPORT(enum json_errors rjson_)(size_t len,
       const unsigned char *cursor,
       struct tree * state) {

#define peek_at(where) cursor[where]
#define SET_STATE_AND_ADVANCE_BY(which_, advance_) \
  state->cur_state = which_; cursor += advance_

    if (state->tokens.stack == NULL) {
        start_state(state, static_stack, static_pool);
    }

    enum json_errors error = JSON_ERROR_NO_ERRORS;
    unsigned const char * final = cursor + len;

    // todo: fully test reentry
    // todo: make ANSI/STDC compatible
    // todo: add jasmine mode? aka not copy strings+numbers ?
    // todo: pedantic mode? (forbid duplicate keys, enforce order, cap integer values to INT_MAX...)
    // todo: test for overflows
    // fixme: check for bounds
    // todo: no memory move mode (simple tokenizer)
    // todo: implement JSON pointer RFC6901
    // todo: implement JSON schema

    for(;;) {
        switch (state->cur_state) {
            case EXPECT_BOM: {
                if(
                        (char)peek_at(0) == '\xEF'
                        && (char)peek_at(1) == '\xBB'
                        && (char)peek_at(2) == '\xBF'
                        ) {
                    SET_STATE_AND_ADVANCE_BY(EXPECT_VALUE, 3);
                } else {
                    SET_STATE_AND_ADVANCE_BY(EXPECT_VALUE, 0);
                }
                break;
            }

            case EXPECT_VALUE:
            {
                if (peek_at(len_whitespace(cursor)+0) == '"') {
                    START_STRING(state, cursor + len_whitespace(cursor));
                    PUSH_STRING(state, (unsigned char[]) {peek_at(len_whitespace(cursor) + 0)}, 1);
                    SET_STATE_AND_ADVANCE_BY(IN_STRING, len_whitespace(cursor) + 1);
                }
                else if (peek_at(len_whitespace(cursor) + 0) == '[') {
                    START_AND_PUSH_TOKEN(state, ARRAY, "[");
                    PUSH_ROOT(state);
                    SET_STATE_AND_ADVANCE_BY(OPEN_ARRAY, len_whitespace(cursor) + 1);
                }
                else if (peek_at(len_whitespace(cursor) + 0) == '{') {
                    START_AND_PUSH_TOKEN(state, OBJECT, "{");
                    PUSH_ROOT(state);
                    SET_STATE_AND_ADVANCE_BY(OPEN_ASSOC, len_whitespace(cursor) + 1);
                }
                else if (in(digit_starters, peek_at(len_whitespace(cursor) + 0))) {
                    START_STRING(state, cursor + len_whitespace(cursor));
                    SET_STATE_AND_ADVANCE_BY(START_NUMBER, len_whitespace(cursor) + 0);
                }
                else if (
                        peek_at(len_whitespace(cursor) + 0) == 't'
                        && peek_at(len_whitespace(cursor) + 1) == 'r'
                        && peek_at(len_whitespace(cursor) + 2) == 'u'
                        && peek_at(len_whitespace(cursor) + 3) == 'e' )
                {
                    START_AND_PUSH_TOKEN(state, JSON_TRUE, "true");
                    SET_STATE_AND_ADVANCE_BY(
                            AFTER_VALUE,
                            len_whitespace(cursor)+sizeof("true") - 1)
                            ;
                }
                else if (peek_at(len_whitespace(cursor) + 0) == 'f'
                         && peek_at(len_whitespace(cursor) + 1) == 'a'
                         && peek_at(len_whitespace(cursor) + 2) == 'l'
                         && peek_at(len_whitespace(cursor) + 3) == 's'
                         && peek_at(len_whitespace(cursor) + 4) == 'e') {
                    START_AND_PUSH_TOKEN(state, JSON_FALSE, "false");
                    SET_STATE_AND_ADVANCE_BY(
                            AFTER_VALUE,
                            len_whitespace(cursor)+sizeof("false") - 1
                            );
                }
                else if (peek_at(len_whitespace(cursor)+0) == 'n'
                         && peek_at(len_whitespace(cursor)+1) == 'u'
                         && peek_at(len_whitespace(cursor)+2) == 'l'
                         && peek_at(len_whitespace(cursor)+3) == 'l') {
                    START_AND_PUSH_TOKEN(state, JSON_NULL, "null");
                    SET_STATE_AND_ADVANCE_BY(
                            AFTER_VALUE,
                            len_whitespace(cursor)+sizeof("null") - 1
                            );
                }
                else if (state->current_support != TOKEN_ROOT) {
                    error = JSON_ERROR_ASSOC_EXPECT_VALUE;
                }
                else if (remaining(final, cursor)) {
                    error = JSON_ERROR_INVALID_CHARACTER;
                }
                else {
                    error = JSON_ERROR_EMPTY;
                }

                break;
            }
            case AFTER_VALUE:
            {
                if (state->current_support == TOKEN_ROOT) {
                    if (remaining(final, cursor + len_whitespace(cursor))) {
                        error = JSON_ERROR_NO_SIBLINGS;
                    } else if(state->mode == JSON1 && state->tokens.stack[state->tokens.stack[state->tokens.max - 1].support_index].kind != OBJECT) {
                        error = JSON_ERROR_JSON1_ONLY_ASSOC_ROOT;
                    } else {
                        goto exit;
                    }
                }
                else if(state->tokens.stack[state->current_support].kind == ARRAY) {
                    SET_STATE_AND_ADVANCE_BY(ARRAY_AFTER_VALUE, 0);
                }
                else if(state->tokens.stack[state->current_support].kind == STRING) {
                    SET_STATE_AND_ADVANCE_BY(ASSOC_AFTER_INNER_VALUE, 0);
                }

                break;
            }
            case OPEN_ARRAY: {
                if (peek_at(len_whitespace(cursor)) == ']') {
                    CLOSE_ROOT(state);
                    SET_STATE_AND_ADVANCE_BY(AFTER_VALUE, len_whitespace(cursor) + 1);
                } else {
                    SET_STATE_AND_ADVANCE_BY(EXPECT_VALUE, 0);
                }

                break;
            }
            case OPEN_ASSOC: {
                if (peek_at(len_whitespace(cursor)) == '}') {
                    CLOSE_ROOT(state);
                    SET_STATE_AND_ADVANCE_BY(AFTER_VALUE, len_whitespace(cursor) + 1);
                } else {
                    SET_STATE_AND_ADVANCE_BY(ASSOC_EXPECT_KEY, 0);
                }
                break;
            }
            case LITERAL_ESCAPE: {
                error = (enum json_errors)(JSON_ERROR_INVALID_ESCAPE_SEQUENCE * !in("\\\"/ubfnrt", peek_at(0)));  /* u"\/ */
                if (in("\\\"/bfnrt", peek_at(0))) {
                    PUSH_STRING(state, (unsigned char[]) {peek_at(0)}, 1);
                    SET_STATE_AND_ADVANCE_BY(IN_STRING, 1);
                }
                else if (peek_at(0) == 'u') {
                    if(!(in(hexdigits, peek_at(1))
                    && in(hexdigits, peek_at(2))
                    && in(hexdigits, peek_at(3))
                    && in(hexdigits, peek_at(4)))
                    ) {
                        error = JSON_ERROR_INCOMPLETE_UNICODE_ESCAPE;
                        break;
                    }
                    PUSH_STRING(state, (char*)cursor, 5);
                    SET_STATE_AND_ADVANCE_BY(IN_STRING, 5);
                    break;
                }
                else {
                    error = JSON_ERROR_INVALID_ESCAPE_SEQUENCE;
                }

                break;
            }
            case IN_STRING: {
                error = (enum json_errors)(JSON_ERROR_UNESCAPED_CONTROL * (peek_at(0) < 0x20));
                PUSH_STRING(state, (unsigned char[]) {peek_at(0)}, error == JSON_ERROR_NO_ERRORS);
                SET_STATE_AND_ADVANCE_BY(
                        (
                                (enum states[]){
                                        IN_STRING,
                                        LITERAL_ESCAPE,
                                        CLOSE_STRING
                                }[in("\\\"", peek_at(0))]),
                        /*state->error == JSON_ERROR_NO_ERRORS);*/
                        1);

                break;
            }

            case START_NUMBER: {
                PUSH_STRING(
                        state,
                        ((unsigned char[]){peek_at(0)}),
                        ((int[]){0, 1}[in("-", peek_at(0))])
                    );
                SET_STATE_AND_ADVANCE_BY(
                    NUMBER_AFTER_MINUS,
                    (
                            (int[]){0, 1}[in("-", peek_at(0))])
                    );

                break;
            }

            case NUMBER_AFTER_MINUS:
            {
                if (peek_at(0) == '0') {
                    PUSH_STRING(state, (unsigned char[]){peek_at(0)}, 1);
                    SET_STATE_AND_ADVANCE_BY(EXPECT_FRACTION, 1);
                } else if (in(digits19, peek_at(0))) {
                    PUSH_STRING(state, (unsigned char[]){peek_at(0)}, 1);
                    SET_STATE_AND_ADVANCE_BY(IN_NUMBER, 1);
                } else {
                    error = JSON_ERROR_INVALID_NUMBER;
                }

                break;
            }
            case IN_NUMBER: {
                PUSH_STRING(
                        state,
                        (unsigned char[]){peek_at(0)},
                        (in(digits, peek_at(0)) != 0)
                );
                SET_STATE_AND_ADVANCE_BY(
                        ((enum states[]){
                            EXPECT_FRACTION,
                            IN_NUMBER}[in(digits, peek_at(0)) != 0]),
                (in(digits, peek_at(0)) != 0)
                );

                break;
            }
            case EXPECT_FRACTION: {
                PUSH_STRING(
                        state,
                        (unsigned char[]){peek_at(0)},
                        (peek_at(0) == '.')
                );
                SET_STATE_AND_ADVANCE_BY(
                        ((enum states[]){
                                EXPECT_EXPONENT,
                                IN_FRACTION}[peek_at(0) == '.']),
                        (peek_at(0) == '.')
                );

                break;
            }

            case EXPECT_EXPONENT: {
                if (peek_at(0) == 'e' || peek_at(0) == 'E') {
                    PUSH_STRING(state, (unsigned char[]){peek_at(0)}, 1);
                    SET_STATE_AND_ADVANCE_BY(EXPONENT_EXPECT_PLUS_MINUS, 1);
                } else {
                    PUSH_STRING_TOKEN(NUMBER, state);
                    SET_STATE_AND_ADVANCE_BY(AFTER_VALUE, 0);
                }

                break;
            }

            case IN_FRACTION: {
                error = (enum json_errors)(JSON_ERROR_INVALID_NUMBER * (in(digits, peek_at(0)) == 0));
                PUSH_STRING(state,
                            (unsigned char[]){peek_at(0)},
                            error == JSON_ERROR_NO_ERRORS);
                SET_STATE_AND_ADVANCE_BY(
                        ((enum states[]){
                            IN_FRACTION,
                            IN_FRACTION_DIGIT
                        }[error == JSON_ERROR_NO_ERRORS]),
                        error == JSON_ERROR_NO_ERRORS);

                break;
            }

            case IN_FRACTION_DIGIT: {
                if (in(digits, peek_at(0))) {
                    PUSH_STRING(state, (unsigned char[]){peek_at(0)}, 1);
                    SET_STATE_AND_ADVANCE_BY(IN_FRACTION_DIGIT, 1);
                } else {
                    SET_STATE_AND_ADVANCE_BY(EXPECT_EXPONENT, 0);
                }

                break;
            }

            case EXPONENT_EXPECT_PLUS_MINUS: {
                if (peek_at(0) == '+' || peek_at(0) == '-') {
                    PUSH_STRING(state, (unsigned char[]){peek_at(0)}, 1);
                    SET_STATE_AND_ADVANCE_BY(EXPECT_EXPONENT_DIGIT, 1);
                } else {
                    SET_STATE_AND_ADVANCE_BY(EXPECT_EXPONENT_DIGIT, 0);
                }

                break;
            }

            case EXPECT_EXPONENT_DIGIT: {
                if (in(digits, peek_at(0))) {
                    PUSH_STRING(state, (unsigned char[]){peek_at(0)}, 1);
                    SET_STATE_AND_ADVANCE_BY(IN_EXPONENT_DIGIT, 1);
                } else {
                    error = JSON_ERROR_INVALID_NUMBER;
                }

                break;
            }

            case IN_EXPONENT_DIGIT: {
                if (in(digits, peek_at(0))) {
                    PUSH_STRING(state, (unsigned char[]) {peek_at(0)}, 1);
                    SET_STATE_AND_ADVANCE_BY(IN_EXPONENT_DIGIT, 1);
                } else {
                    PUSH_STRING_TOKEN(NUMBER, state);
                    SET_STATE_AND_ADVANCE_BY(AFTER_VALUE, len_whitespace(cursor) + 0);
                }

                break;
            }

            case ARRAY_AFTER_VALUE: {
                if(peek_at(len_whitespace(cursor)) == ',') {
                    SET_STATE_AND_ADVANCE_BY(EXPECT_VALUE, len_whitespace(cursor) + 1);
                } else if(peek_at(len_whitespace(cursor) + 0) == ']') {
                    CLOSE_ROOT(state);
                    SET_STATE_AND_ADVANCE_BY(AFTER_VALUE, len_whitespace(cursor) + 1);
                } else {
                    error = JSON_ERROR_INVALID_CHARACTER_IN_ARRAY;
                }

                break;
            }

            case ASSOC_EXPECT_KEY: {
                if(peek_at(len_whitespace(cursor) + 0) == '"') {
                    START_STRING(state, cursor + len_whitespace(cursor));
                    PUSH_STRING(state, (unsigned char[]) {peek_at(len_whitespace(cursor) + 0)}, 1);
                    SET_STATE_AND_ADVANCE_BY(IN_STRING, len_whitespace(cursor) + 1);
                } else {
                    error = JSON_ERROR_ASSOC_EXPECT_STRING_A_KEY;
                }
                break;
            }

            case CLOSE_STRING: {  /* fixme: non advancing state */
                PUSH_STRING_TOKEN(STRING, state);
                if ((state->tokens.stack)[state->current_support].kind == OBJECT) {
                    PUSH_ROOT(state);
                    SET_STATE_AND_ADVANCE_BY(ASSOC_EXPECT_COLON, 0);
                } else {
                    SET_STATE_AND_ADVANCE_BY(AFTER_VALUE, 0);
                }

                break;
            }

            case ASSOC_EXPECT_COLON: {
                error = (enum json_errors)(JSON_ERROR_ASSOC_EXPECT_COLON * (peek_at(len_whitespace(cursor)) != ':'));
                SET_STATE_AND_ADVANCE_BY(
                        ((enum states[]){
                            ASSOC_EXPECT_COLON,
                            EXPECT_VALUE}[error == JSON_ERROR_NO_ERRORS]),
                (len_whitespace(cursor) + 1) * (error == JSON_ERROR_NO_ERRORS));
                break;
            }

            case ASSOC_AFTER_INNER_VALUE: {
                if(peek_at(len_whitespace(cursor)) == ',') {
                    CLOSE_ROOT(state);
                    SET_STATE_AND_ADVANCE_BY(ASSOC_EXPECT_KEY, len_whitespace(cursor) + 1);
                } else if (peek_at(len_whitespace(cursor)) == '}'){
                    CLOSE_ROOT(state);
                    CLOSE_ROOT(state);
                    SET_STATE_AND_ADVANCE_BY(AFTER_VALUE, len_whitespace(cursor) +1);
                } else {
                    error = JSON_ERROR_ASSOC_EXPECT_VALUE;
                }

                break;
            }
        }

        if(error != JSON_ERROR_NO_ERRORS) {
            goto exit;
        }
    }

    exit: return error;
#undef peek_at
#undef SET_STATE_AND_ADVANCE_BY
}

static char ident_s[0x80];
static char * print_ident(int depth, int indent) {
    if (!depth) return (char*)"";
    cs_memset(ident_s, ' ', (size_t)(indent * depth));
    ident_s[(size_t)(indent * depth)] = '\0';
    return ident_s;
}

/**
 * Returns support of a node, until the root or virtual
 * root is met (when printing from a pointer).
 */
static inline struct token *
support(struct tokens* stack,
        struct token* cur,
        struct token* virtual_root) {
    if(!cur) return NULL;
    if(cur->support_index == TOKEN_ROOT) return NULL;
    if(cur == virtual_root) return NULL;
    return &stack->stack[cur->support_index];
}

static inline struct token *
next_print(struct tokens* toks, struct token* start, struct token* cur) {
    struct token* sup = support(toks, cur, start);
    struct token* next;

    if((next = next_child_(toks, cur, NULL))) {
        return next;
    } /* have child */
    siblings:
    if(sup && (next = next_child_(toks, sup, cur))) {
        return next;
    } /* have sibling */
    if(sup) {
        cur = sup;
        sup = support(toks, sup, start);
        goto siblings;
    }
    return NULL;
}

/* whether descendant is on the same branch as root */
static int
on_branch(struct tokens* stack,
          struct token* root,
          struct token* descendant) {
    if(descendant == NULL) return 0;
    if (descendant == root) return 1;
    struct token* cur= descendant;
    while (cur->support_index != TOKEN_ROOT) {
        cur = &stack->stack[cur->support_index];
        if (cur == root) return 1;
    }
    return 0;
}


static int is_last_child(
        struct tokens* toks, struct token* tok, struct token* sup,
                struct token* virtual_root) {
    /* either a container without children and parent,
     * last element of an array, or of an object */
    if(!sup) {
        return 1;
    }
    if (sup->kind == ARRAY) {
        return next_child_(toks, sup, tok) == NULL;
    }
    if (sup->kind == STRING) {
        struct token* sup2 = support(toks, sup, virtual_root);
        return sup2 && next_child_(toks, sup2, sup) == NULL;
    }
    return 0;
}

static unsigned char output_[STRING_POOL_SIZE];

EXPORT(unsigned char * restrict
to_string_)(
        struct tokens * restrict tokens,
        struct token * start,
        int indent, unsigned char* sink,
        int sink_size) {

#define cat_raw(where, string) ( \
    cs_memcpy((where), (string), cs_strlen((string))), cs_strlen((string)) \
)
    unsigned char * output = output_;
    if(sink) {
        output = sink;
    } else {
        sink_size = sizeof output_;
    }
//#ifndef NDEBUG
    cs_memset(output_, 0, sizeof sink_size);
//#endif

    size_t cursor = 0;

    struct token *stack = tokens->stack;
    if (start == NULL) {
        start = &stack[0];
    }

    int depth = 0;
    struct token * tok;
    for(tok = start;
        tok != NULL && aindex(tokens->stack, tok) < tokens->max;
        tok = next_print(tokens, start, tok)
    ) {
        if (stack[tok->support_index].kind != STRING) {
            cursor += cat_raw(output + cursor, print_ident(depth, indent));
        }

        size_t len = tok_len(tok);

        cs_memcpy(output+cursor, tok->address, len);
        cursor += len;

        if (stack[tok->support_index].kind == OBJECT && tok->kind == STRING) {
            cursor += cat_raw(output + cursor, indent ? ": " :  ":");
        }

        if(tok->kind == ARRAY || tok->kind == OBJECT) {
            cursor += cat_raw(output + cursor, !indent ? "" : "\n");
            depth++;
        }

        if(next_child_(tokens, tok, NULL) != NULL) {
            continue; /* if we have children */
        }
        struct token * sup;
        struct token * cur;
        for(cur = tok, sup = support(tokens, tok, start); /* close groups ]} */
            cur && on_branch(tokens, start, tok) && is_last_child(tokens, cur, sup, start); // && sup != start;
            cur = sup, sup = support(tokens, sup, start)) {
            if(
                sup &&
                (sup->kind == ARRAY || sup->kind == OBJECT) &&
                sup->kind != STRING
            ) {
                cursor += cat_raw(output + cursor, !indent ? "" : "\n");
                --depth, depth < 0 ? depth = 0 : 0;
                cursor += cat_raw(output + cursor,
                                  print_ident(depth,
                                              indent));
            }

            char end[2] = {"\0]}"[in((const char[]){ARRAY, STRING},sup ? sup->kind : 0)], '\0'};
            if(!sup && next_child_(tokens, tok, NULL) == NULL) {
                end[0] = "\0]}"[in((const char[]){ARRAY, OBJECT}, tok->kind)];
            } // special case for isolated arrays/objects
            cursor += cat_raw(output + cursor, end);
        }

        if (sup && (sup->kind == ARRAY || sup->kind == STRING)
            && tok->kind != OBJECT && tok->kind != ARRAY
            && !(sup->kind == STRING && sup == start)
            ) {
            cursor += cat_raw(output + cursor,
                              indent ? ",\n" : ",");
        }
    }

    output[cursor] = '\0';
    return output;
#undef cat
#undef cat_raw

}
