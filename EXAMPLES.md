To turn the following C object into JSON

```C
struct foo = {
  .foo = "bar",
  .array = {1, 2, 3},
  .question = true
};
```

```C
#define CISSON_IMPLEMENTATION
#include "cisson.h"

int main(void) {
    struct tree tree = {0};
    
    /* every token we push will be bound to the current root */
    START_AND_PUSH_TOKEN(&tree, OBJECT, "{");
    PUSH_ROOT(&tree);/* next tokens will have { as root */
      START_AND_PUSH_TOKEN(&tree, STRING, "\"foo\"");
      PUSH_ROOT(&tree); /* next token will be bound to the key */
        START_AND_PUSH_TOKEN(&tree, STRING, "\"bar\"");
      CLOSE_ROOT(&tree);
    /* now the root is no more the key but the object */
     START_AND_PUSH_TOKEN(&tree, STRING, "\"array\"");
     PUSH_ROOT(&tree);
       START_AND_PUSH_TOKEN(&tree, ARRAY, "[");
       PUSH_ROOT(&tree);
         START_AND_PUSH_TOKEN(&tree, NUMBER, "1");
         START_AND_PUSH_TOKEN(&tree, NUMBER, "2");
         START_AND_PUSH_TOKEN(&tree, NUMBER, "3");
       CLOSE_ROOT(&tree); /* the array */
     CLOSE_ROOT(&tree); /* the object property */
       START_AND_PUSH_TOKEN(&tree, STRING, "\"question\"");
       PUSH_ROOT(&tree);
         START_AND_PUSH_TOKEN(&tree, TRUE, "true");
    puts(to_string(&tree)); /* {"foo":"bar","array":[1,2,4],"question":true} */
}
```



```C

#define CISSON_IMPLEMENTATION
#include <cisson.h>

int main() {
    /** 
    * This is how to manage ourselves our memory. 
    * 
    * By default, a static stack of tokens and a 
    * static pool of characters are used.
    **/
    
    struct tree tree1 = { 0 };
    struct tree tree2 = { 0 };
    
    struct token stack1[40];
    struct token stack2[40];
    unsigned char pool1[400];
    unsigned char pool2[400];
    
    start_state(&tree1, stack1, pool1);
    start_state(&tree2, stack2, pool2);
    /* set up trees to work with custom memory. 
     * It can also be used to reset an existing tree and
     * reduce allocations. */
    
    rjson("[1, 2, 3, [4, 5, 6], [7, 8, 9]", &tree1);
    rjson("{\"foo\": 1}", &tree2);
    
    insert_token(&tree2, "\"array\"", query(&tree2, ""));
    /* tree2 is now {"bar":1,"array"} (not valid JSON). */
    move(&tree1, query(&tree1, "/1"), query(&tree1, "/4"));
    /*  tree1 is now [1, 3, [4, 5, 6], [7, 8, 9, 2]. */
    inject(to_string_pointer( &tree1, query(&tree1, "/3")),
    &tree2, query(&tree2, "/array/<"));
    /* to_string_pointer() extracts JSON from anywhere
     * within a tree.
     * 
     * inject() injects JSON text anywhere inside a tree.
     * 
     * tree2 is now {"bar":1,"array":[7,8,9,2]}
     * 
     * The < bit targets the "array" node itself rather 
     * than its descendant.
     * 
     * query(..., "/array) targets [7,8,9,2]
     * query(..., "/array/<) targets "array"
     * */
    delete(query(&tree1, "/3"));
    /* tree1 is now [1, 3, [4, 5, 6]] */
    
    rename_string(&tree2, query(&tree2, "/foo/<"), "bar");
    /* rename_string() works like
     * 
     * insert_token(&tree2, "\"bar\"", query(&tree2, ""));
     * move(&tree2, query(&tree2, "/foo"), query(&tree2, "/bar/<");
     * delete(&tree2, query(&tree2, "/foo/<");
     */
    
    puts(to_string_compact(&tree1)); /* [1,3,[4,5,6]] */
    puts(to_string_compact(&tree2)); /* {"bar":1,"array":[7,8,9,2]} */
    
    /* print every value of /array */
    struct token *root, *cur;
    for(root = query(&tree2, "/array"),
        cur = next_child(&tree2, root, NULL);
        cur != NULL;
        cur = next_child(&tree2, root, cur)) {
        printf(" %s", to_string_pointer(&tree2, cur));
    }
    /* 7 8 9 2 */
    
    /* print every root object value */
    for(root = query(&tree2, ""),
        cur = next_child(&tree2, root, NULL);
        cur != NULL;
        cur = next_child(&tree2, root, cur)) {
        printf(" %s", to_string_pointer(&tree2, next_child(&tree2, cur, NULL)));
    }
    /* 1 [7,8,9,2] */
    
    /* print every root object key */
    for(root = query(&tree2, ""),
        cur = next_child(&tree2, root, NULL);
        cur != NULL;
        cur = next_child(&tree2, root, cur)) {
        printf(" %s", to_string_pointer(&tree2, cur));
    }
    /* "bar":1 "array":[7,8,9,2] */
    
    /* Json printing functions will print every 
     * token bound to the root we provide.
     * To print the keys without their children, we have
     * to peek at the raw tree object and extract the strings 
     * they point at, this isn't supported but if you really
     * need that feature you can use this recipe. */
    for(root = query(&tree2, ""),
        cur = next_child(&tree2, root, NULL);
        cur != NULL;
        cur = next_child(&tree2, root, cur)) {
        printf(
            " %.*s",
            *(string_size_type_raw*)
            ((char*)cur->address - sizeof string_size_type),
            cur->address
        );
    }
    /* "bar" "array" */

}

```

## API

`rjson()` reads JSON and creates a tree to represent it.
```c
struct tree tree = { 0 };
rjson("[10, 20, 30, 40]", &tree);
```
`to_string()` writes JSON inside a static buffer whose size
can be customized at compilation time and by default is
`0x2000` bytes long. `to_string_sink()` lets you declare
where you want the JSON to be written.
```c
puts(to_string(&tree)); /* [10, 20, 30, 40] */ 
char sink[0x100];
puts(to_string_sink(&tree, query(&tree, "/2"), sink, 0x100); /* 30 */
```
`inject()` reads JSON and injects it inside an existing tree,
together with `query()` to target where.
```c
inject("{\"foo\": 12}", &tree, query(&tree, "/2"));
/* [10, 20, {"foo": 12}, 30, 40] */
```
`to_string_pointer()`extracts JSON from a tree, from the
place pointed by `query()`.
```c
puts(to_string_pointer(&tree, query(&tree, "/2/foo"))); /* 12 */
puts(to_string_pointer(&tree, query(&tree, "/2/foo/<"))); /* "foo":12 */
puts(to_string_pointer(&tree, query(&tree, "/3"))); /* 30 */
puts(to_string_pointer(&tree, query(&tree, ""))); /* [10, 20, {"foo": 12}, 30, 40] */
```
`next_child()` browses JSON compound values in an iterator-like
fashion.
```c
struct token *root, *cur;
for(root = query(&tree, ""),
    cur = next_child(&tree, root, NULL);
    cur != NULL;
    cur = next_child(&tree, root, cur)) {
    printf(" %s", to_string_pointer(&state2, cur));
}
/* 10 20 {"foo":12} 30 40 */
```
