Cisson is a small self-contained JSON library in C. 
It parses JSON into an abstract tree and serializes trees 
into JSON. It lets you manipulate trees in various 
ways and build them from scratch.

It supports all of JSON, comes with memory management features
that are convenient if you don't want to allocate yourself
memory. It is extensively reentrant and made to generate,
peek into, print and resume the building of JSON documents at any pace.

## Requirements
* C, tested with GCC and MSVC. 
* Optional support
for [Cmake](https://cmake.org/), you must have
version 3.14 minimum to use cmake features.

## Install
```c
#define CISSON_IMPLEMENTATION
#include "cisson.h"
```
The preprocessor will copy `cisson.h` in your file, and
there is nothing more to do on your side. You can include
cisson.h as many times as you want, but you must define
`CISSON_IMPLEMENTATION` only once in all your code base.

***

Without the implementation included in the header.
```c
#include "json.h"
```
In that case you have to compile the implementation 
`njson.c`, and link it to the executable.

***

With cmake.
```cmake
add_subdirectory(cisson)
# Bake it directly in your program
# JSON_FILES is a list that contains the files
add_executable(your_executable ${YOUR_SOURCES} ${JSON_FILES})
# sjson is the static lib target.
target_link_libraries(your_executable_target sjson)
# xjson is the dynamic lib target.
target_link_libraries(your_executable_target xjson)
```

## Examples

```c
#define CISSON_IMPLEMENTATION
#include "cisson.h"
#include <stdio.h>

int main() {
    struct tree tree = {0};
    /* Creates a tree */
    rjson("{\"mon\":[],\"tue\":[]}", &tree);
    /* read JSON and turn it into a tree .*/

    struct token* mon = query(&tree, "/mon")
    struct token* tue = query(&tree, "/tue");
    /* JSON pointers, see rfc6901 */
    
    char buf[4];
    int j;
    for (j = 0; j < 20; j++) {
        sprintf(buf, "%d", j);
        insert_token(&tree, buf, j % 2 ? mon : tue);
    }
    
    puts(to_string(&tree));
    /**
     * {
     *   "mon":[1,3,5,7,9,11,13,15,17,19],
     *   "tue":[0,2,4,6,8,10,12,14,16,18]
     * }
     */

}
```

## Preprocessor options
* `WANT_LIBC` includes the standard C library instead of
using drop-in replacement functions.
* `STRING_POOL_SIZE` and `MAX_TOKENS` let you customize
  your work memory size.
