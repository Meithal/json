/* Automatically generated to be used as a single header replacement
 * of the full library, do not edit. */
#ifndef JSON_JSON_H
#define JSON_JSON_H

#ifdef __cplusplus
#define C_BIT "C"
#else
#define C_BIT
#endif


#ifdef WANT_LIBC
#ifdef __cplusplus
#include<cstdio>
#include<cstring>
#include<cstddef>
#else
#include<stdio.h>
#include<string.h>
#include<stddef.h>
#endif
#endif

#if (!(defined(_WIN32) || defined(_WIN64)) \
|| defined(__CYGWIN__) \
|| defined(__MINGW32__) \
|| defined(__MINGW64__))
/* outside of windows or inside cygwin */
#define HAS_VLA
#define va_(val) val
#else
#define va_(val)
#endif
#ifdef __cplusplus
#undef HAS_VLA
#undef va_
#define va_(val)
#endif

#if defined(__STDC_VERSION__) && __STDC_VERSION__ >= 199901L && !defined(FORCE_ANSI)
#define c99
#else
#define restrict
#endif


#ifdef HAS_VLA
#define EXPORT(symbol) extern C_BIT symbol
#else
#define EXPORT(symbol) extern C_BIT __declspec(dllexport) symbol
#endif

#if !defined(WANT_LIBC)   /* No libC */
typedef unsigned long size_t
#define NULL ((void *)0)
typedef signed long ptrdiff_t
#endif

#ifdef __cplusplus
#undef NULL
#define NULL nullptr
#endif


#ifndef WANT_LIBC
/* from MuslC */
EXPORT(size_t cisson_strlen)(const unsigned char *s)
{
    const unsigned char *a = s;
    for (; *s; s++);
    return s-a;
}
#define cs_strlen(s) cisson_strlen((const unsigned char*)(s))

EXPORT(void * cisson_memset)(void *dest, int c, size_t n)
{
    unsigned char *s = (unsigned char*)dest;

    for (; n; n--, s++) *s = c;
    return dest;
}
#define cs_memset(dest, val, repeat) (cisson_memset((dest), (val), (repeat)))

EXPORT(void *cisson_memcpy)(void *restrict dest, const void *restrict src, size_t n)
{
    unsigned char *d = (unsigned char *)dest;
    const unsigned char *s = (unsigned char *)src;

    for (; n; n--) *d++ = *s++;
    return dest;
}
#define cs_memcpy(dest, val, repeat) (cisson_memcpy((dest), (val), (repeat)))

EXPORT(int cisson_memcmp)(const void *vl, const void *vr, size_t n)
{
	const unsigned char *l=(unsigned char *)vl, *r=(unsigned char *)vr;
	for (; n && *l == *r; n--, l++, r++);
	return n ? *l-*r : 0;
}
#define cs_memcmp(v1, v2, size) (cisson_memcmp((v1), (v2), (size)))

EXPORT(void *cisson_memmove)(void *dest, const void *src, size_t n)
{
	char *d = (char*)dest;
	const char *s = (char*)src;

	if (d==s) return d;
	if (s-d-n <= -2*n) return cisson_memcpy(d, s, n);

	if (d<s) {
		for (; n; n--) *d++ = *s++;
	} else {
		while (n) n--, d[n] = s[n];
	}

	return dest;
}
#define cs_memmove(dest, src, size) (cisson_memmove((dest), (src), (size)))

#else
#define cs_strlen(s) strlen(s)
#define cs_memset(dest, val, repeat) memset((dest), (val), (repeat))
#define cs_memcpy(dest, val, repeat) memcpy((dest), (val), (repeat))
#define cs_memcmp(v1, v2, size) memcmp((v1), (v2), (size))
#define cs_memmove(dest, src, size) memmove((dest), (src), (size))

#endif  /* WANT_LIBC */


/*****************************************************/


#ifndef STRING_POOL_SIZE
    #define STRING_POOL_SIZE 0x2000
#endif
#ifndef MAX_TOKENS
    #define MAX_TOKENS 0x200
#endif

/**
 * Json structures are stored as a flat array of objects holding
 * a link to their parent whose value is their index in that array, index zero being the root
 * Json doesn't require json arrays elements to have an order so sibling data is not stored.
 */

#define WHITESPACE \
  X(TABULATION, '\t') \
  X(LINE_FEED, '\n') \
  X(CARRIAGE_RETURN, '\r') \
  X(SPACE, ' ')

#define ERRORS \
  X(JSON_ERROR_NO_ERRORS, "No errors found.") \
  X(JSON_ERROR_INVALID_CHARACTER, "Found an unknown token.") \
  X(JSON_ERROR_TWO_OBJECTS_HAVE_SAME_PARENT, "Two values have the same parent.") \
  X(JSON_ERROR_EMPTY, "A JSON document can't be empty.") \
  X(JSON_ERROR_INVALID_ESCAPE_SEQUENCE, "Invalid escape sequence.")                       \
  X(JSON_ERROR_INVALID_NUMBER, "Malformed number.")              \
  X(JSON_ERROR_INVALID_CHARACTER_IN_ARRAY, "Invalid character in array.") \
  X(JSON_ERROR_ASSOC_EXPECT_STRING_A_KEY, "A JSON object key must be a quoted string.") \
  X(JSON_ERROR_ASSOC_EXPECT_COLON, "Missing colon after object key.") \
  X(JSON_ERROR_ASSOC_EXPECT_VALUE, "Missing value after JSON object key.")  \
  X(JSON_ERROR_NO_SIBLINGS, "Only Arrays and Objects can have sibling descendants.")      \
  X(JSON_ERROR_JSON1_ONLY_ASSOC_ROOT, "JSON1 only allows objects as root element.")  \
  X(JSON_ERROR_UTF16_NOT_SUPPORTED_YET, "Code points greater than 0x0800 not supported yet.") \
  X(JSON_ERROR_INCOMPLETE_UNICODE_ESCAPE, "Incomplete unicode character sequence.") \
  X(JSON_ERROR_UNESCAPED_CONTROL, "Control characters must be escaped.") \

#define X(a, b) a,
enum whitespace_tokens { WHITESPACE };
enum json_errors{ ERRORS };
#undef X

#ifdef c99
#define X(a, b) [(a)] = (b),
#else
#define X(a, b) (b),
#endif
static char whitespaces[] = {
    WHITESPACE
    '\0',
};

static char const * json_errors[] = {
    ERRORS
};
#undef X

#undef WHITESPACE
#undef ERRORS

static char digit_starters[] = "-0123456789";
static char digits[] = "0123456789";
static char digits19[] = "123456789";
static char hexdigits[] = "0123456789abcdefABCDEF";

enum states {
    EXPECT_BOM = 0,
    EXPECT_VALUE,
    AFTER_VALUE,
    OPEN_ARRAY,
    OPEN_ASSOC,
    LITERAL_ESCAPE,
    IN_STRING,
    START_NUMBER,
    NUMBER_AFTER_MINUS,
    IN_NUMBER,
    EXPECT_FRACTION,
    EXPECT_EXPONENT,
    IN_FRACTION,
    IN_FRACTION_DIGIT,
    EXPONENT_EXPECT_PLUS_MINUS,
    EXPECT_EXPONENT_DIGIT,
    IN_EXPONENT_DIGIT,
    ARRAY_AFTER_VALUE,
    ASSOC_EXPECT_KEY,
    CLOSE_STRING,
    ASSOC_EXPECT_COLON,
    ASSOC_AFTER_INNER_VALUE
};

enum json_mode {
    JSON2,
    JSON1,
};

#define string_size_type_raw unsigned int
#define string_size_type (string_size_type_raw)
#define tok_len(token) ( \
(token)->length          \
? (token)->length        \
: *(string_size_type_raw *)((token)->address - sizeof string_size_type))

enum kind {
    UNSET,
    JSON_TRUE,
    JSON_FALSE,
    JSON_NULL,
    STRING,
    NUMBER,
    ARRAY,
    OBJECT,
};

struct token {
    enum kind kind;
    int support_index;
    unsigned char * address;
    string_size_type_raw length;
};

#define TOKEN_ROOT (-1)
#define TOKEN_DELETED (-2)

struct tokens {
    struct token *stack;
    int max;
    int tangled;
};

struct tree {
    int do_not_touch_; /* when compiling in C++, if the
    * first field is an enum, the whole thing can't be
    * { 0 }  initialized, this is just to make sure the
    * first field will never be an enum. */
    enum states cur_state;
    int current_support;
    struct tokens tokens;
    struct strings {
        unsigned char *pool;
        unsigned int cursor;
    } strings;
    enum json_mode mode;
    int no_copy;
    const unsigned char *cur_string_start;
    int cur_string_len;
};

static struct token static_stack[MAX_TOKENS];
static unsigned char static_pool[STRING_POOL_SIZE];

/* State maintenance */
EXPORT(void start_state)(
        struct tree * state,
        struct token *stack,
        unsigned char *pool);
EXPORT(struct token*query_)(
        struct tree * state,
        size_t length,
        unsigned char query[va_(length)]);
#define query(state, string) query_((state), cs_strlen((char*)(string)), (unsigned char*)(string))
EXPORT(struct token* next_child_)(
        struct tokens *  tokens,
        struct token * root,
        struct token * current);
#define next_child(tree, root, current) next_child_(&(tree)->tokens, root, current)
EXPORT(int aindex)(struct token* stack, struct token* which);
static inline struct token *
        tok_at(struct tokens* toks, int idx) {
    return &toks->stack[idx];
}
/* Parsing */
EXPORT(enum json_errors rjson_)(
        size_t len,
        const unsigned char *cursor,
        struct tree * state
);
#define rjson(text, state) rjson_(cs_strlen(((char*)(text))), (unsigned char*)(text), (state))
EXPORT(enum json_errors inject_)(
        size_t len,
        unsigned char text[va_(len)],
        struct tree * state,
        struct token * where
);
#define inject(text, state, where) inject_(cs_strlen(((char*)(text))), (unsigned char*)(text), (state), (where))
/* Output */

#define to_string(state_) (char * restrict)to_string_(&(state_)->tokens, NULL, 2, NULL, -1)
#define to_string_compact(state_) (char * restrict)to_string_(&(state_)->tokens, NULL, 0, NULL, -1)
#define to_string_pointer(state_, pointer_) (char * restrict)to_string_(&(state_)->tokens, pointer_, 0, NULL, -1)
#define to_string_sink(state_, pointer_, sink, size) (char * restrict)to_string_(&(state_)->tokens, pointer_, 0, sink, size)
EXPORT(unsigned char* restrict to_string_)(
        struct tokens * restrict tokens,
        struct token * start,
        int compact, 
        unsigned char* sink,
        int sink_size);
/* Building */
EXPORT(void start_string)(unsigned int *, unsigned char [STRING_POOL_SIZE]);
EXPORT(void push_string)(const unsigned int * restrict cursor, unsigned char * restrict pool, unsigned char* restrict string, size_t length);
EXPORT(void close_root)(struct token * restrict, int * restrict);
EXPORT(void push_root)(int * restrict, const int * restrict);
EXPORT(void push_token_kind)(enum kind kind, void *restrict address
                            , struct tokens *tokens, int root_index);
EXPORT(void delete_token)(struct token* which);
EXPORT(void move)(struct tree* state, struct token* which, struct token* where);
EXPORT(void rename_string_)(struct tree* state, struct token* which, size_t len, const char* new_name);
#define rename_string(state, which, new_name) rename_string_(state, which, cs_strlen((char*)(new_name)), new_name)
/* EZ JSON */
EXPORT(void insert_token_)(struct tree * state, unsigned char *token, struct token* root);
#define insert_token(state, token, root) insert_token_((state), (unsigned char*)(token), root)
#define push_token(state, token) insert_token_((state), (unsigned char*)(token), &(state)->tokens.stack[(state)->current_support])
EXPORT(void stream_tokens_)(struct tree * state, struct token * where, char separator, unsigned char *stream, size_t length);
#define stream_tokens(state, sep, stream) stream_tokens_( \
    (state),                                              \
    &(state)->tokens.stack[(state)->current_support],     \
    (sep), (unsigned char*)(stream), cs_strlen(((char*)(stream))))
#define stream_into(state, where, sep, stream) stream_tokens_((state), (where), (sep), (stream), cs_strlen(((char*)(stream))))
#define START_STRING(state_, string_) \
    (state_)->no_copy        \
    ? (state_)->cur_string_start = (const unsigned char *)(string_) \
    , (state_)->cur_string_len = 0\
    : (start_string(&(state_)->strings.cursor, (state_)->strings.pool), 0)
#define PUSH_STRING(state_, string_, length_) \
    (state_)->no_copy                         \
    ? (state_)->cur_string_len += (length_) \
    : (push_string(                             \
        &(state_)->strings.cursor,             \
        (state_)->strings.pool,                \
        (unsigned char*)(string_),                             \
        (length_)), 0)
#define CLOSE_ROOT(state_) close_root((*(state_)).tokens.stack, &(*(state_)).current_support)
#define PUSH_ROOT(state_) push_root(&(state_)->current_support, &(state_)->tokens.max)
#define PUSH_TOKEN(kind_, address_, state_) \
    push_token_kind(                             \
        (kind_),                            \
        (void*)(address_),                         \
        &(state_)->tokens,                  \
        (state_)->current_support)
#define INSERT_TOKEN(kind_, address_, state_, root) \
    push_token_kind(                        \
        (kind_),                            \
        (address_),                         \
        &(state_)->tokens,                  \
        (root))
#define PUSH_STRING_TOKEN(kind_, state_) \
    do {                                          \
        if((state_)->no_copy) {                    \
            PUSH_TOKEN(                        \
                (kind_),                             \
                (state_)->cur_string_start - sizeof string_size_type, \
                (state_));               \
                (state_)->tokens.stack[(state_)->tokens.max - 1].length \
                = (state_)->cur_string_len; \
        } else {                                             \
            PUSH_TOKEN(                          \
                (kind_),                             \
                (state_)->strings.pool + (state_)->strings.cursor, \
                (state_)                             \
            ); \
        } \
    }while(0)

#define INSERT_STRING_TOKEN(kind_, state_, root) \
    INSERT_TOKEN(                                \
        (kind_),                                 \
        (state_)->strings.pool + (state_)->strings.cursor, \
        (state_),                                \
        (root)                                   \
    )

#define START_AND_PUSH_TOKEN(state, kind, string) \
    do {                                          \
        START_STRING(state, string);               \
        PUSH_STRING((state), string, (cs_strlen ((char*)(string)))); \
        PUSH_STRING_TOKEN(kind, state);       \
    }while(0)

#define START_AND_INSERT_TOKEN(state, kind, string, root) \
    START_STRING(state, string);               \
    PUSH_STRING((state), (string), (cs_strlen ((char*)(string)))); \
    INSERT_STRING_TOKEN((kind), (state), (root))
/* __/ */


#undef c99
/* Stripped header guard. */

#endif // JSON_JSON_H /* Automatically added header guard. */
#ifdef CISSON_IMPLEMENTATION

static inline size_t
in(const char* restrict hay, unsigned char needle) {
    if (needle == '\0') return 0;
    const char * begin = hay;
    for (;*hay; hay++) {
        if(*hay == needle) return (hay - begin) + 1;
    }
    return 0;
}

static inline size_t
len_whitespace(unsigned const char * restrict string) {
    int count = 0;
    while(in(whitespaces, string[count])) {
        count++;
    }
    return count;
}

static inline size_t
remaining(unsigned const char * restrict max, unsigned const char * restrict where) {
    return max - where;
}

EXPORT(void
start_string)(unsigned int * restrict cursor,
             unsigned char pool[STRING_POOL_SIZE]) {
    *cursor += (string_size_type_raw)(pool[*cursor]) + sizeof string_size_type;
    cs_memset(&pool[*cursor], 0, sizeof string_size_type);
}

EXPORT(void
push_string)(const unsigned int *cursor,
            unsigned char *pool,
            unsigned char* string,
            size_t length) {
    /* todo: memmove if we insert */
    string_size_type_raw* sh = (string_size_type_raw*)(void*)&pool[*cursor];
    cs_memcpy(
            pool + *cursor + sizeof string_size_type + *sh,
            string,
            length);
    *sh += (string_size_type_raw)length;
}

EXPORT(void
close_root)(struct token * tokens,
        int * root_index) {
    *root_index = tokens[*root_index].support_index;
}

EXPORT(void
push_root)(int * root_index, const int * token_cursor) {
    *root_index = *token_cursor - 1;
}

static struct token *
tree_root(struct tokens * tokens, struct token * from) {
    while(from->support_index != TOKEN_ROOT &&
        aindex(
                tokens->stack,
                from
        ) != from->support_index
    ) {from = &tokens->stack[from->support_index];}

    return from;
}

EXPORT(void
push_token_kind)(
        enum kind kind,
        void * address,
        struct tokens *tokens,
        int root_index) {

    struct token tok = {
        .kind=kind,
        .support_index=root_index,
        .address=(unsigned char*)address + sizeof string_size_type
    };
    tokens->stack[(tokens->max)++] = tok;
}

EXPORT(void
insert_token_)(struct tree * state, unsigned char *token, struct token* root) {
    int target_root = aindex(state->tokens.stack, root);
    if(target_root != state->current_support) {
        state->tokens.tangled = 1;
    }
    if(token[0] == '>') {
        int i = 0;

        while (token[i] && token[i] == '>') {
            CLOSE_ROOT(state);
            i++;
        }
        return;
    }

    enum kind kind[] = {
            UNSET, JSON_TRUE, JSON_FALSE, JSON_NULL, ARRAY, OBJECT,
            NUMBER, NUMBER, NUMBER, NUMBER, NUMBER, NUMBER,
            NUMBER, NUMBER, NUMBER, NUMBER, NUMBER, STRING};
    enum kind kind2 = kind[in("tfn[{-0123456789\"", token[0])];
    if(kind2 != UNSET) {
        START_AND_INSERT_TOKEN(state, kind2, token, target_root);
    }
    if(in("{[:", token[0])) PUSH_ROOT(state);
    if(kind2 == STRING
        && tok_at(&state->tokens,state->current_support)->kind == OBJECT) {
        PUSH_ROOT(state);
    }
}

EXPORT(void stream_tokens_)(
        struct tree * state, struct token * where,
        char separator, unsigned char *stream, size_t length) {
    size_t i = 0;
    int old_root = state->current_support;
    state->current_support = (int)(where - state->tokens.stack);
    if(old_root != state->current_support) {
        state->tokens.tangled = 1;
    }
    while (i < length) {
        size_t token_length = 0;
        while (i + token_length < length && stream[i + token_length] != separator) {
            token_length++;
        }
        stream[i + token_length] = '\0';
        push_token(state, &stream[i]);
        i = i + token_length + sizeof separator;
    }
    state->current_support = old_root;
}

EXPORT(void start_state)(
        struct tree * state,
        struct token *stack,
        unsigned char *pool
        ) {
    state->cur_state = EXPECT_BOM;
    state->tokens.stack = stack;
    state->strings.pool = pool;
    state->current_support = -1;
    state->tokens.max = 0;
    state->tokens.tangled = 0;
    state->strings.cursor = 0;
}

/**
 * Returns the next sibling from the given root and the given node
 * If the given node is NULL, returns the first child
 * If last child or no child exists, returns NULL.
 */
EXPORT(struct token* next_child_)(
        struct tokens * tokens,
        struct token * root,
        struct token * current) {
    int root_idx = aindex(tokens->stack, root);
    int start = (root_idx + 1) % tokens->max;
    if (current)
        start = (aindex(tokens->stack, current) + 1) % tokens->max;

    if (start <= root_idx && !tokens->tangled) return NULL;

    int i;
    if(tokens->tangled)
        for( /* if tokens are tangled we must test all of them, O(n) */
                i = start;
                i != root_idx;
                i++, i%=tokens->max) {
            if (tokens->stack[i].support_index == root_idx) {
                return &tokens->stack[i];
            }
        }
    else /* if tokens are untangled, break on first sibling with different parent */
        for(i = start; i < tokens->max; i++) /* we still have to browse over nested arrays */
            if (tokens->stack[i].support_index == root_idx) {
                return &tokens->stack[i];
            }
    return NULL;
}

EXPORT(struct token*
query_from)(
        struct tree * state, size_t length,
        unsigned char query[va_(length)],
        struct token * cursor) {
    /* todo : unescape ~0, ~1, ~2 */
    size_t i = 0;

    while (i < length) {
        if (query[i] == '/') {
            if(cursor->kind != OBJECT && cursor->kind != ARRAY) {
                return NULL;
            }
            i++;
            size_t token_length = 0u;
            while (i + token_length < length && query[i + token_length] != '/') {
                token_length++;
            }
            unsigned char buffer[0x80] = { (unsigned char)('"' * (cursor->kind == OBJECT)) };
            cs_memcpy(&buffer[(ptrdiff_t)1 * (cursor->kind == OBJECT)], &query[i], token_length);
            buffer[1 + token_length] = '"' * (cursor->kind == OBJECT);

            if (cursor->kind == ARRAY) {
                int index = 0;
                int j;
                for (j = 0; buffer[j]; j++) {
                    index *= 10;
                    index += buffer[j] - '0';
                }
                struct token * cur = NULL;
                do {
                    cur = next_child(state, cursor, cur);
                } while (index--);
                cursor = cur;
            } else if (cursor->kind == OBJECT) {
                int index = state->tokens.max;
                struct token * cur = NULL;
                do {
                    cur = next_child(state, cursor, cur);
                    if (cs_memcmp(cur->address,
                                  buffer,
                                  token_length + 2) == 0) {
                        if((i + token_length + 1 < length &&
                            query[i+token_length] == '/' && query[i+token_length+1] == '<')) {
                            return cur;
                        }
                            cursor = next_child(
                                    state,
                                    cur,
                                    NULL);

                        break;
                    }
                } while (index--);
            }
            i = i + token_length - 1;
        }
        i++;
    }

    return cursor;
}

EXPORT(struct token*
query_)(struct tree * tree,
        size_t length,
        unsigned char query[va_(length)]) {
    return query_from(
        tree, length, query,
        tree_root(&tree->tokens, &tree->tokens.stack[0])
    );
}

EXPORT(int aindex)(struct token* stack, struct token* which) {
    return (int)(which - stack);
}

EXPORT(void delete_token)(struct token* which) {
    /* todo: this should make the tree tangled but we don't
     * get a reference to it here. */
    which->support_index = -2;
}

EXPORT(void move)(
        struct tree* state, struct token* which, struct token* where) {
    int new_idx = aindex(state->tokens.stack, where);
    if(which->support_index != new_idx) {
        state->tokens.tangled = 1;
    }
    which->support_index = new_idx;
}

EXPORT(void rename_string_)(
        struct tree* state, struct token* which, size_t len, const char* new_name) {
    START_STRING(state, new_name);
    PUSH_STRING(state, "\"", 1);
    PUSH_STRING(state, new_name, len);
    PUSH_STRING(state, "\"", 1);
    which->address = state->strings.pool + state->strings.cursor + sizeof string_size_type;
}

EXPORT(enum json_errors inject_)(size_t len,
       unsigned char text[va_(len)],
       struct tree * state,
       struct token * where) {
    enum states old_state = state->cur_state;
    int old_root = state->current_support;
    state->current_support = (int)(where - (state->tokens.stack));

    if(old_root != 0
    && state->current_support > 1
    && old_root != state->current_support) {
        /* because of the root token we can get false reading here */
        state->tokens.tangled = 1;
    }
    state->cur_state = EXPECT_BOM;
    enum json_errors error = rjson_(
            len, (unsigned char *) text, state);
    state->cur_state = old_state;
    state->current_support = old_root;
    return error;
}

EXPORT(enum json_errors rjson_)(size_t len,
       const unsigned char *cursor,
       struct tree * state) {

#define peek_at(where) cursor[where]
#define SET_STATE_AND_ADVANCE_BY(which_, advance_) \
  state->cur_state = which_; cursor += advance_

    if (state->tokens.stack == NULL) {
        start_state(state, static_stack, static_pool);
    }

    enum json_errors error = JSON_ERROR_NO_ERRORS;
    unsigned const char * final = cursor + len;

    // todo: fully test reentry
    // todo: make ANSI/STDC compatible
    // todo: add jasmine mode? aka not copy strings+numbers ?
    // todo: pedantic mode? (forbid duplicate keys, enforce order, cap integer values to INT_MAX...)
    // todo: test for overflows
    // fixme: check for bounds
    // todo: no memory move mode (simple tokenizer)
    // todo: implement JSON pointer RFC6901
    // todo: implement JSON schema

    for(;;) {
        switch (state->cur_state) {
            case EXPECT_BOM: {
                if(
                        (char)peek_at(0) == '\xEF'
                        && (char)peek_at(1) == '\xBB'
                        && (char)peek_at(2) == '\xBF'
                        ) {
                    SET_STATE_AND_ADVANCE_BY(EXPECT_VALUE, 3);
                } else {
                    SET_STATE_AND_ADVANCE_BY(EXPECT_VALUE, 0);
                }
                break;
            }

            case EXPECT_VALUE:
            {
                if (peek_at(len_whitespace(cursor)+0) == '"') {
                    START_STRING(state, cursor + len_whitespace(cursor));
                    PUSH_STRING(state, (unsigned char[]) {peek_at(len_whitespace(cursor) + 0)}, 1);
                    SET_STATE_AND_ADVANCE_BY(IN_STRING, len_whitespace(cursor) + 1);
                }
                else if (peek_at(len_whitespace(cursor) + 0) == '[') {
                    START_AND_PUSH_TOKEN(state, ARRAY, "[");
                    PUSH_ROOT(state);
                    SET_STATE_AND_ADVANCE_BY(OPEN_ARRAY, len_whitespace(cursor) + 1);
                }
                else if (peek_at(len_whitespace(cursor) + 0) == '{') {
                    START_AND_PUSH_TOKEN(state, OBJECT, "{");
                    PUSH_ROOT(state);
                    SET_STATE_AND_ADVANCE_BY(OPEN_ASSOC, len_whitespace(cursor) + 1);
                }
                else if (in(digit_starters, peek_at(len_whitespace(cursor) + 0))) {
                    START_STRING(state, cursor + len_whitespace(cursor));
                    SET_STATE_AND_ADVANCE_BY(START_NUMBER, len_whitespace(cursor) + 0);
                }
                else if (
                        peek_at(len_whitespace(cursor) + 0) == 't'
                        && peek_at(len_whitespace(cursor) + 1) == 'r'
                        && peek_at(len_whitespace(cursor) + 2) == 'u'
                        && peek_at(len_whitespace(cursor) + 3) == 'e' )
                {
                    START_AND_PUSH_TOKEN(state, JSON_TRUE, "true");
                    SET_STATE_AND_ADVANCE_BY(
                            AFTER_VALUE,
                            len_whitespace(cursor)+sizeof("true") - 1)
                            ;
                }
                else if (peek_at(len_whitespace(cursor) + 0) == 'f'
                         && peek_at(len_whitespace(cursor) + 1) == 'a'
                         && peek_at(len_whitespace(cursor) + 2) == 'l'
                         && peek_at(len_whitespace(cursor) + 3) == 's'
                         && peek_at(len_whitespace(cursor) + 4) == 'e') {
                    START_AND_PUSH_TOKEN(state, JSON_FALSE, "false");
                    SET_STATE_AND_ADVANCE_BY(
                            AFTER_VALUE,
                            len_whitespace(cursor)+sizeof("false") - 1
                            );
                }
                else if (peek_at(len_whitespace(cursor)+0) == 'n'
                         && peek_at(len_whitespace(cursor)+1) == 'u'
                         && peek_at(len_whitespace(cursor)+2) == 'l'
                         && peek_at(len_whitespace(cursor)+3) == 'l') {
                    START_AND_PUSH_TOKEN(state, JSON_NULL, "null");
                    SET_STATE_AND_ADVANCE_BY(
                            AFTER_VALUE,
                            len_whitespace(cursor)+sizeof("null") - 1
                            );
                }
                else if (state->current_support != TOKEN_ROOT) {
                    error = JSON_ERROR_ASSOC_EXPECT_VALUE;
                }
                else if (remaining(final, cursor)) {
                    error = JSON_ERROR_INVALID_CHARACTER;
                }
                else {
                    error = JSON_ERROR_EMPTY;
                }

                break;
            }
            case AFTER_VALUE:
            {
                if (state->current_support == TOKEN_ROOT) {
                    if (remaining(final, cursor + len_whitespace(cursor))) {
                        error = JSON_ERROR_NO_SIBLINGS;
                    } else if(state->mode == JSON1 && state->tokens.stack[state->tokens.stack[state->tokens.max - 1].support_index].kind != OBJECT) {
                        error = JSON_ERROR_JSON1_ONLY_ASSOC_ROOT;
                    } else {
                        goto exit;
                    }
                }
                else if(state->tokens.stack[state->current_support].kind == ARRAY) {
                    SET_STATE_AND_ADVANCE_BY(ARRAY_AFTER_VALUE, 0);
                }
                else if(state->tokens.stack[state->current_support].kind == STRING) {
                    SET_STATE_AND_ADVANCE_BY(ASSOC_AFTER_INNER_VALUE, 0);
                }

                break;
            }
            case OPEN_ARRAY: {
                if (peek_at(len_whitespace(cursor)) == ']') {
                    CLOSE_ROOT(state);
                    SET_STATE_AND_ADVANCE_BY(AFTER_VALUE, len_whitespace(cursor) + 1);
                } else {
                    SET_STATE_AND_ADVANCE_BY(EXPECT_VALUE, 0);
                }

                break;
            }
            case OPEN_ASSOC: {
                if (peek_at(len_whitespace(cursor)) == '}') {
                    CLOSE_ROOT(state);
                    SET_STATE_AND_ADVANCE_BY(AFTER_VALUE, len_whitespace(cursor) + 1);
                } else {
                    SET_STATE_AND_ADVANCE_BY(ASSOC_EXPECT_KEY, 0);
                }
                break;
            }
            case LITERAL_ESCAPE: {
                error = (enum json_errors)(JSON_ERROR_INVALID_ESCAPE_SEQUENCE * !in("\\\"/ubfnrt", peek_at(0)));  /* u"\/ */
                if (in("\\\"/bfnrt", peek_at(0))) {
                    PUSH_STRING(state, (unsigned char[]) {peek_at(0)}, 1);
                    SET_STATE_AND_ADVANCE_BY(IN_STRING, 1);
                }
                else if (peek_at(0) == 'u') {
                    if(!(in(hexdigits, peek_at(1))
                    && in(hexdigits, peek_at(2))
                    && in(hexdigits, peek_at(3))
                    && in(hexdigits, peek_at(4)))
                    ) {
                        error = JSON_ERROR_INCOMPLETE_UNICODE_ESCAPE;
                        break;
                    }
                    PUSH_STRING(state, (char*)cursor, 5);
                    SET_STATE_AND_ADVANCE_BY(IN_STRING, 5);
                    break;
                }
                else {
                    error = JSON_ERROR_INVALID_ESCAPE_SEQUENCE;
                }

                break;
            }
            case IN_STRING: {
                error = (enum json_errors)(JSON_ERROR_UNESCAPED_CONTROL * (peek_at(0) < 0x20));
                PUSH_STRING(state, (unsigned char[]) {peek_at(0)}, error == JSON_ERROR_NO_ERRORS);
                SET_STATE_AND_ADVANCE_BY(
                        (
                                (enum states[]){
                                        IN_STRING,
                                        LITERAL_ESCAPE,
                                        CLOSE_STRING
                                }[in("\\\"", peek_at(0))]),
                        /*state->error == JSON_ERROR_NO_ERRORS);*/
                        1);

                break;
            }

            case START_NUMBER: {
                PUSH_STRING(
                        state,
                        ((unsigned char[]){peek_at(0)}),
                        ((int[]){0, 1}[in("-", peek_at(0))])
                    );
                SET_STATE_AND_ADVANCE_BY(
                    NUMBER_AFTER_MINUS,
                    (
                            (int[]){0, 1}[in("-", peek_at(0))])
                    );

                break;
            }

            case NUMBER_AFTER_MINUS:
            {
                if (peek_at(0) == '0') {
                    PUSH_STRING(state, (unsigned char[]){peek_at(0)}, 1);
                    SET_STATE_AND_ADVANCE_BY(EXPECT_FRACTION, 1);
                } else if (in(digits19, peek_at(0))) {
                    PUSH_STRING(state, (unsigned char[]){peek_at(0)}, 1);
                    SET_STATE_AND_ADVANCE_BY(IN_NUMBER, 1);
                } else {
                    error = JSON_ERROR_INVALID_NUMBER;
                }

                break;
            }
            case IN_NUMBER: {
                PUSH_STRING(
                        state,
                        (unsigned char[]){peek_at(0)},
                        (in(digits, peek_at(0)) != 0)
                );
                SET_STATE_AND_ADVANCE_BY(
                        ((enum states[]){
                            EXPECT_FRACTION,
                            IN_NUMBER}[in(digits, peek_at(0)) != 0]),
                (in(digits, peek_at(0)) != 0)
                );

                break;
            }
            case EXPECT_FRACTION: {
                PUSH_STRING(
                        state,
                        (unsigned char[]){peek_at(0)},
                        (peek_at(0) == '.')
                );
                SET_STATE_AND_ADVANCE_BY(
                        ((enum states[]){
                                EXPECT_EXPONENT,
                                IN_FRACTION}[peek_at(0) == '.']),
                        (peek_at(0) == '.')
                );

                break;
            }

            case EXPECT_EXPONENT: {
                if (peek_at(0) == 'e' || peek_at(0) == 'E') {
                    PUSH_STRING(state, (unsigned char[]){peek_at(0)}, 1);
                    SET_STATE_AND_ADVANCE_BY(EXPONENT_EXPECT_PLUS_MINUS, 1);
                } else {
                    PUSH_STRING_TOKEN(NUMBER, state);
                    SET_STATE_AND_ADVANCE_BY(AFTER_VALUE, 0);
                }

                break;
            }

            case IN_FRACTION: {
                error = (enum json_errors)(JSON_ERROR_INVALID_NUMBER * (in(digits, peek_at(0)) == 0));
                PUSH_STRING(state,
                            (unsigned char[]){peek_at(0)},
                            error == JSON_ERROR_NO_ERRORS);
                SET_STATE_AND_ADVANCE_BY(
                        ((enum states[]){
                            IN_FRACTION,
                            IN_FRACTION_DIGIT
                        }[error == JSON_ERROR_NO_ERRORS]),
                        error == JSON_ERROR_NO_ERRORS);

                break;
            }

            case IN_FRACTION_DIGIT: {
                if (in(digits, peek_at(0))) {
                    PUSH_STRING(state, (unsigned char[]){peek_at(0)}, 1);
                    SET_STATE_AND_ADVANCE_BY(IN_FRACTION_DIGIT, 1);
                } else {
                    SET_STATE_AND_ADVANCE_BY(EXPECT_EXPONENT, 0);
                }

                break;
            }

            case EXPONENT_EXPECT_PLUS_MINUS: {
                if (peek_at(0) == '+' || peek_at(0) == '-') {
                    PUSH_STRING(state, (unsigned char[]){peek_at(0)}, 1);
                    SET_STATE_AND_ADVANCE_BY(EXPECT_EXPONENT_DIGIT, 1);
                } else {
                    SET_STATE_AND_ADVANCE_BY(EXPECT_EXPONENT_DIGIT, 0);
                }

                break;
            }

            case EXPECT_EXPONENT_DIGIT: {
                if (in(digits, peek_at(0))) {
                    PUSH_STRING(state, (unsigned char[]){peek_at(0)}, 1);
                    SET_STATE_AND_ADVANCE_BY(IN_EXPONENT_DIGIT, 1);
                } else {
                    error = JSON_ERROR_INVALID_NUMBER;
                }

                break;
            }

            case IN_EXPONENT_DIGIT: {
                if (in(digits, peek_at(0))) {
                    PUSH_STRING(state, (unsigned char[]) {peek_at(0)}, 1);
                    SET_STATE_AND_ADVANCE_BY(IN_EXPONENT_DIGIT, 1);
                } else {
                    PUSH_STRING_TOKEN(NUMBER, state);
                    SET_STATE_AND_ADVANCE_BY(AFTER_VALUE, len_whitespace(cursor) + 0);
                }

                break;
            }

            case ARRAY_AFTER_VALUE: {
                if(peek_at(len_whitespace(cursor)) == ',') {
                    SET_STATE_AND_ADVANCE_BY(EXPECT_VALUE, len_whitespace(cursor) + 1);
                } else if(peek_at(len_whitespace(cursor) + 0) == ']') {
                    CLOSE_ROOT(state);
                    SET_STATE_AND_ADVANCE_BY(AFTER_VALUE, len_whitespace(cursor) + 1);
                } else {
                    error = JSON_ERROR_INVALID_CHARACTER_IN_ARRAY;
                }

                break;
            }

            case ASSOC_EXPECT_KEY: {
                if(peek_at(len_whitespace(cursor) + 0) == '"') {
                    START_STRING(state, cursor + len_whitespace(cursor));
                    PUSH_STRING(state, (unsigned char[]) {peek_at(len_whitespace(cursor) + 0)}, 1);
                    SET_STATE_AND_ADVANCE_BY(IN_STRING, len_whitespace(cursor) + 1);
                } else {
                    error = JSON_ERROR_ASSOC_EXPECT_STRING_A_KEY;
                }
                break;
            }

            case CLOSE_STRING: {  /* fixme: non advancing state */
                PUSH_STRING_TOKEN(STRING, state);
                if ((state->tokens.stack)[state->current_support].kind == OBJECT) {
                    PUSH_ROOT(state);
                    SET_STATE_AND_ADVANCE_BY(ASSOC_EXPECT_COLON, 0);
                } else {
                    SET_STATE_AND_ADVANCE_BY(AFTER_VALUE, 0);
                }

                break;
            }

            case ASSOC_EXPECT_COLON: {
                error = (enum json_errors)(JSON_ERROR_ASSOC_EXPECT_COLON * (peek_at(len_whitespace(cursor)) != ':'));
                SET_STATE_AND_ADVANCE_BY(
                        ((enum states[]){
                            ASSOC_EXPECT_COLON,
                            EXPECT_VALUE}[error == JSON_ERROR_NO_ERRORS]),
                (len_whitespace(cursor) + 1) * (error == JSON_ERROR_NO_ERRORS));
                break;
            }

            case ASSOC_AFTER_INNER_VALUE: {
                if(peek_at(len_whitespace(cursor)) == ',') {
                    CLOSE_ROOT(state);
                    SET_STATE_AND_ADVANCE_BY(ASSOC_EXPECT_KEY, len_whitespace(cursor) + 1);
                } else if (peek_at(len_whitespace(cursor)) == '}'){
                    CLOSE_ROOT(state);
                    CLOSE_ROOT(state);
                    SET_STATE_AND_ADVANCE_BY(AFTER_VALUE, len_whitespace(cursor) +1);
                } else {
                    error = JSON_ERROR_ASSOC_EXPECT_VALUE;
                }

                break;
            }
        }

        if(error != JSON_ERROR_NO_ERRORS) {
            goto exit;
        }
    }

    exit: return error;
#undef peek_at
#undef SET_STATE_AND_ADVANCE_BY
}

static char ident_s[0x80];
static char * print_ident(int depth, int indent) {
    if (!depth) return (char*)"";
    cs_memset(ident_s, ' ', (size_t)(indent * depth));
    ident_s[(size_t)(indent * depth)] = '\0';
    return ident_s;
}

/**
 * Returns support of a node, until the root or virtual
 * root is met (when printing from a pointer).
 */
static inline struct token *
support(struct tokens* stack,
        struct token* cur,
        struct token* virtual_root) {
    if(!cur) return NULL;
    if(cur->support_index == TOKEN_ROOT) return NULL;
    if(cur == virtual_root) return NULL;
    return &stack->stack[cur->support_index];
}

static inline struct token *
next_print(struct tokens* toks, struct token* start, struct token* cur) {
    struct token* sup = support(toks, cur, start);
    struct token* next;

    if((next = next_child_(toks, cur, NULL))) {
        return next;
    } /* have child */
    siblings:
    if(sup && (next = next_child_(toks, sup, cur))) {
        return next;
    } /* have sibling */
    if(sup) {
        cur = sup;
        sup = support(toks, sup, start);
        goto siblings;
    }
    return NULL;
}

/* whether descendant is on the same branch as root */
static int
on_branch(struct tokens* stack,
          struct token* root,
          struct token* descendant) {
    if(descendant == NULL) return 0;
    if (descendant == root) return 1;
    struct token* cur= descendant;
    while (cur->support_index != TOKEN_ROOT) {
        cur = &stack->stack[cur->support_index];
        if (cur == root) return 1;
    }
    return 0;
}


static int is_last_child(
        struct tokens* toks, struct token* tok, struct token* sup,
                struct token* virtual_root) {
    /* either a container without children and parent,
     * last element of an array, or of an object */
    if(!sup) {
        return 1;
    }
    if (sup->kind == ARRAY) {
        return next_child_(toks, sup, tok) == NULL;
    }
    if (sup->kind == STRING) {
        struct token* sup2 = support(toks, sup, virtual_root);
        return sup2 && next_child_(toks, sup2, sup) == NULL;
    }
    return 0;
}

static unsigned char output_[STRING_POOL_SIZE];

EXPORT(unsigned char * restrict
to_string_)(
        struct tokens * restrict tokens,
        struct token * start,
        int indent, unsigned char* sink,
        int sink_size) {

#define cat_raw(where, string) ( \
    cs_memcpy((where), (string), cs_strlen((string))), cs_strlen((string)) \
)
    unsigned char * output = output_;
    if(sink) {
        output = sink;
    } else {
        sink_size = sizeof output_;
    }
//#ifndef NDEBUG
    cs_memset(output_, 0, sizeof sink_size);
//#endif

    size_t cursor = 0;

    struct token *stack = tokens->stack;
    if (start == NULL) {
        start = &stack[0];
    }

    int depth = 0;
    struct token * tok;
    for(tok = start;
        tok != NULL && aindex(tokens->stack, tok) < tokens->max;
        tok = next_print(tokens, start, tok)
    ) {
        if (stack[tok->support_index].kind != STRING) {
            cursor += cat_raw(output + cursor, print_ident(depth, indent));
        }

        size_t len = tok_len(tok);

        cs_memcpy(output+cursor, tok->address, len);
        cursor += len;

        if (stack[tok->support_index].kind == OBJECT && tok->kind == STRING) {
            cursor += cat_raw(output + cursor, indent ? ": " :  ":");
        }

        if(tok->kind == ARRAY || tok->kind == OBJECT) {
            cursor += cat_raw(output + cursor, !indent ? "" : "\n");
            depth++;
        }

        if(next_child_(tokens, tok, NULL) != NULL) {
            continue; /* if we have children */
        }
        struct token * sup;
        struct token * cur;
        for(cur = tok, sup = support(tokens, tok, start); /* close groups ]} */
            cur && on_branch(tokens, start, tok) && is_last_child(tokens, cur, sup, start); // && sup != start;
            cur = sup, sup = support(tokens, sup, start)) {
            if(
                sup &&
                (sup->kind == ARRAY || sup->kind == OBJECT) &&
                sup->kind != STRING
            ) {
                cursor += cat_raw(output + cursor, !indent ? "" : "\n");
                --depth, depth < 0 ? depth = 0 : 0;
                cursor += cat_raw(output + cursor,
                                  print_ident(depth,
                                              indent));
            }

            char end[2] = {"\0]}"[in((const char[]){ARRAY, STRING},sup ? sup->kind : 0)], '\0'};
            if(!sup && next_child_(tokens, tok, NULL) == NULL) {
                end[0] = "\0]}"[in((const char[]){ARRAY, OBJECT}, tok->kind)];
            } // special case for isolated arrays/objects
            cursor += cat_raw(output + cursor, end);
        }

        if (sup && (sup->kind == ARRAY || sup->kind == STRING)
            && tok->kind != OBJECT && tok->kind != ARRAY
            && !(sup->kind == STRING && sup == start)
            ) {
            cursor += cat_raw(output + cursor,
                              indent ? ",\n" : ",");
        }
    }

    output[cursor] = '\0';
    return output;
#undef cat
#undef cat_raw

}

#endif  // CISSON_IMPLEMENTATION
#undef va_
#undef HAS_VLA