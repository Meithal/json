#ifndef JSON_JSON_H
#define JSON_JSON_H

#ifdef __cplusplus
#define C_BIT "C"
#else
#define C_BIT
#endif


#ifdef WANT_LIBC
#ifdef __cplusplus
#include<cstdio>
#include<cstring>
#include<cstddef>
#else
#include<stdio.h>
#include<string.h>
#include<stddef.h>
#endif
#endif

#if (!(defined(_WIN32) || defined(_WIN64)) \
|| defined(__CYGWIN__) \
|| defined(__MINGW32__) \
|| defined(__MINGW64__))
/* outside of windows or inside cygwin */
#define HAS_VLA
#define va_(val) val
#else
#define va_(val)
#endif
#ifdef __cplusplus
#undef HAS_VLA
#undef va_
#define va_(val)
#endif

#if defined(__STDC_VERSION__) && __STDC_VERSION__ >= 199901L && !defined(FORCE_ANSI)
#define c99
#else
#define restrict
#endif


#ifdef HAS_VLA
#define EXPORT(symbol) extern C_BIT symbol
#else
#define EXPORT(symbol) extern C_BIT __declspec(dllexport) symbol
#endif

#if !defined(WANT_LIBC)   /* No libC */
typedef unsigned long size_t
#define NULL ((void *)0)
typedef signed long ptrdiff_t
#endif

#ifdef __cplusplus
#undef NULL
#define NULL nullptr
#endif


#ifndef WANT_LIBC
/* from MuslC */
EXPORT(size_t cisson_strlen)(const unsigned char *s)
{
    const unsigned char *a = s;
    for (; *s; s++);
    return s-a;
}
#define cs_strlen(s) cisson_strlen((const unsigned char*)(s))

EXPORT(void * cisson_memset)(void *dest, int c, size_t n)
{
    unsigned char *s = (unsigned char*)dest;

    for (; n; n--, s++) *s = c;
    return dest;
}
#define cs_memset(dest, val, repeat) (cisson_memset((dest), (val), (repeat)))

EXPORT(void *cisson_memcpy)(void *restrict dest, const void *restrict src, size_t n)
{
    unsigned char *d = (unsigned char *)dest;
    const unsigned char *s = (unsigned char *)src;

    for (; n; n--) *d++ = *s++;
    return dest;
}
#define cs_memcpy(dest, val, repeat) (cisson_memcpy((dest), (val), (repeat)))

EXPORT(int cisson_memcmp)(const void *vl, const void *vr, size_t n)
{
	const unsigned char *l=(unsigned char *)vl, *r=(unsigned char *)vr;
	for (; n && *l == *r; n--, l++, r++);
	return n ? *l-*r : 0;
}
#define cs_memcmp(v1, v2, size) (cisson_memcmp((v1), (v2), (size)))

EXPORT(void *cisson_memmove)(void *dest, const void *src, size_t n)
{
	char *d = (char*)dest;
	const char *s = (char*)src;

	if (d==s) return d;
	if (s-d-n <= -2*n) return cisson_memcpy(d, s, n);

	if (d<s) {
		for (; n; n--) *d++ = *s++;
	} else {
		while (n) n--, d[n] = s[n];
	}

	return dest;
}
#define cs_memmove(dest, src, size) (cisson_memmove((dest), (src), (size)))

#else
#define cs_strlen(s) strlen(s)
#define cs_memset(dest, val, repeat) memset((dest), (val), (repeat))
#define cs_memcpy(dest, val, repeat) memcpy((dest), (val), (repeat))
#define cs_memcmp(v1, v2, size) memcmp((v1), (v2), (size))
#define cs_memmove(dest, src, size) memmove((dest), (src), (size))

#endif  /* WANT_LIBC */


/*****************************************************/


#ifndef STRING_POOL_SIZE
    #define STRING_POOL_SIZE 0x2000
#endif
#ifndef MAX_TOKENS
    #define MAX_TOKENS 0x200
#endif

/**
 * Json structures are stored as a flat array of objects holding
 * a link to their parent whose value is their index in that array, index zero being the root
 * Json doesn't require json arrays elements to have an order so sibling data is not stored.
 */

#define WHITESPACE \
  X(TABULATION, '\t') \
  X(LINE_FEED, '\n') \
  X(CARRIAGE_RETURN, '\r') \
  X(SPACE, ' ')

#define ERRORS \
  X(JSON_ERROR_NO_ERRORS, "No errors found.") \
  X(JSON_ERROR_INVALID_CHARACTER, "Found an unknown token.") \
  X(JSON_ERROR_TWO_OBJECTS_HAVE_SAME_PARENT, "Two values have the same parent.") \
  X(JSON_ERROR_EMPTY, "A JSON document can't be empty.") \
  X(JSON_ERROR_INVALID_ESCAPE_SEQUENCE, "Invalid escape sequence.")                       \
  X(JSON_ERROR_INVALID_NUMBER, "Malformed number.")              \
  X(JSON_ERROR_INVALID_CHARACTER_IN_ARRAY, "Invalid character in array.") \
  X(JSON_ERROR_ASSOC_EXPECT_STRING_A_KEY, "A JSON object key must be a quoted string.") \
  X(JSON_ERROR_ASSOC_EXPECT_COLON, "Missing colon after object key.") \
  X(JSON_ERROR_ASSOC_EXPECT_VALUE, "Missing value after JSON object key.")  \
  X(JSON_ERROR_NO_SIBLINGS, "Only Arrays and Objects can have sibling descendants.")      \
  X(JSON_ERROR_JSON1_ONLY_ASSOC_ROOT, "JSON1 only allows objects as root element.")  \
  X(JSON_ERROR_UTF16_NOT_SUPPORTED_YET, "Code points greater than 0x0800 not supported yet.") \
  X(JSON_ERROR_INCOMPLETE_UNICODE_ESCAPE, "Incomplete unicode character sequence.") \
  X(JSON_ERROR_UNESCAPED_CONTROL, "Control characters must be escaped.") \

#define X(a, b) a,
enum whitespace_tokens { WHITESPACE };
enum json_errors{ ERRORS };
#undef X

#ifdef c99
#define X(a, b) [(a)] = (b),
#else
#define X(a, b) (b),
#endif
static char whitespaces[] = {
    WHITESPACE
    '\0',
};

static char const * json_errors[] = {
    ERRORS
};
#undef X

#undef WHITESPACE
#undef ERRORS

static char digit_starters[] = "-0123456789";
static char digits[] = "0123456789";
static char digits19[] = "123456789";
static char hexdigits[] = "0123456789abcdefABCDEF";

enum states {
    EXPECT_BOM = 0,
    EXPECT_VALUE,
    AFTER_VALUE,
    OPEN_ARRAY,
    OPEN_ASSOC,
    LITERAL_ESCAPE,
    IN_STRING,
    START_NUMBER,
    NUMBER_AFTER_MINUS,
    IN_NUMBER,
    EXPECT_FRACTION,
    EXPECT_EXPONENT,
    IN_FRACTION,
    IN_FRACTION_DIGIT,
    EXPONENT_EXPECT_PLUS_MINUS,
    EXPECT_EXPONENT_DIGIT,
    IN_EXPONENT_DIGIT,
    ARRAY_AFTER_VALUE,
    ASSOC_EXPECT_KEY,
    CLOSE_STRING,
    ASSOC_EXPECT_COLON,
    ASSOC_AFTER_INNER_VALUE
};

enum json_mode {
    JSON2,
    JSON1,
};

#define string_size_type_raw unsigned int
#define string_size_type (string_size_type_raw)
#define tok_len(token) ( \
(token)->length          \
? (token)->length        \
: *(string_size_type_raw *)((token)->address - sizeof string_size_type))

enum kind {
    UNSET,
    JSON_TRUE,
    JSON_FALSE,
    JSON_NULL,
    STRING,
    NUMBER,
    ARRAY,
    OBJECT,
};

struct token {
    enum kind kind;
    int support_index;
    unsigned char * address;
    string_size_type_raw length;
};

#define TOKEN_ROOT (-1)
#define TOKEN_DELETED (-2)

struct tokens {
    struct token *stack;
    int max;
    int tangled;
};

struct tree {
    int do_not_touch_; /* when compiling in C++, if the
    * first field is an enum, the whole thing can't be
    * { 0 }  initialized, this is just to make sure the
    * first field will never be an enum. */
    enum states cur_state;
    int current_support;
    struct tokens tokens;
    struct strings {
        unsigned char *pool;
        unsigned int cursor;
    } strings;
    enum json_mode mode;
    int no_copy;
    const unsigned char *cur_string_start;
    int cur_string_len;
};

static struct token static_stack[MAX_TOKENS];
static unsigned char static_pool[STRING_POOL_SIZE];

/* State maintenance */
EXPORT(void start_state)(
        struct tree * state,
        struct token *stack,
        unsigned char *pool);
EXPORT(struct token*query_)(
        struct tree * state,
        size_t length,
        unsigned char query[va_(length)]);
#define query(state, string) query_((state), cs_strlen((char*)(string)), (unsigned char*)(string))
EXPORT(struct token* next_child_)(
        struct tokens *  tokens,
        struct token * root,
        struct token * current);
#define next_child(tree, root, current) next_child_(&(tree)->tokens, root, current)
EXPORT(int aindex)(struct token* stack, struct token* which);
static inline struct token *
        tok_at(struct tokens* toks, int idx) {
    return &toks->stack[idx];
}
/* Parsing */
EXPORT(enum json_errors rjson_)(
        size_t len,
        const unsigned char *cursor,
        struct tree * state
);
#define rjson(text, state) rjson_(cs_strlen(((char*)(text))), (unsigned char*)(text), (state))
EXPORT(enum json_errors inject_)(
        size_t len,
        unsigned char text[va_(len)],
        struct tree * state,
        struct token * where
);
#define inject(text, state, where) inject_(cs_strlen(((char*)(text))), (unsigned char*)(text), (state), (where))
/* Output */

#define to_string(state_) (char * restrict)to_string_(&(state_)->tokens, NULL, 2, NULL, -1)
#define to_string_compact(state_) (char * restrict)to_string_(&(state_)->tokens, NULL, 0, NULL, -1)
#define to_string_pointer(state_, pointer_) (char * restrict)to_string_(&(state_)->tokens, pointer_, 0, NULL, -1)
#define to_string_sink(state_, pointer_, sink, size) (char * restrict)to_string_(&(state_)->tokens, pointer_, 0, sink, size)
EXPORT(unsigned char* restrict to_string_)(
        struct tokens * restrict tokens,
        struct token * start,
        int compact, 
        unsigned char* sink,
        int sink_size);
/* Building */
EXPORT(void start_string)(unsigned int *, unsigned char [STRING_POOL_SIZE]);
EXPORT(void push_string)(const unsigned int * restrict cursor, unsigned char * restrict pool, unsigned char* restrict string, size_t length);
EXPORT(void close_root)(struct token * restrict, int * restrict);
EXPORT(void push_root)(int * restrict, const int * restrict);
EXPORT(void push_token_kind)(enum kind kind, void *restrict address
                            , struct tokens *tokens, int root_index);
EXPORT(void delete_token)(struct token* which);
EXPORT(void move)(struct tree* state, struct token* which, struct token* where);
EXPORT(void rename_string_)(struct tree* state, struct token* which, size_t len, const char* new_name);
#define rename_string(state, which, new_name) rename_string_(state, which, cs_strlen((char*)(new_name)), new_name)
/* EZ JSON */
EXPORT(void insert_token_)(struct tree * state, unsigned char *token, struct token* root);
#define insert_token(state, token, root) insert_token_((state), (unsigned char*)(token), root)
#define push_token(state, token) insert_token_((state), (unsigned char*)(token), &(state)->tokens.stack[(state)->current_support])
EXPORT(void stream_tokens_)(struct tree * state, struct token * where, char separator, unsigned char *stream, size_t length);
#define stream_tokens(state, sep, stream) stream_tokens_( \
    (state),                                              \
    &(state)->tokens.stack[(state)->current_support],     \
    (sep), (unsigned char*)(stream), cs_strlen(((char*)(stream))))
#define stream_into(state, where, sep, stream) stream_tokens_((state), (where), (sep), (stream), cs_strlen(((char*)(stream))))
#define START_STRING(state_, string_) \
    (state_)->no_copy        \
    ? (state_)->cur_string_start = (const unsigned char *)(string_) \
    , (state_)->cur_string_len = 0\
    : (start_string(&(state_)->strings.cursor, (state_)->strings.pool), 0)
#define PUSH_STRING(state_, string_, length_) \
    (state_)->no_copy                         \
    ? (state_)->cur_string_len += (length_) \
    : (push_string(                             \
        &(state_)->strings.cursor,             \
        (state_)->strings.pool,                \
        (unsigned char*)(string_),                             \
        (length_)), 0)
#define CLOSE_ROOT(state_) close_root((*(state_)).tokens.stack, &(*(state_)).current_support)
#define PUSH_ROOT(state_) push_root(&(state_)->current_support, &(state_)->tokens.max)
#define PUSH_TOKEN(kind_, address_, state_) \
    push_token_kind(                             \
        (kind_),                            \
        (void*)(address_),                         \
        &(state_)->tokens,                  \
        (state_)->current_support)
#define INSERT_TOKEN(kind_, address_, state_, root) \
    push_token_kind(                        \
        (kind_),                            \
        (address_),                         \
        &(state_)->tokens,                  \
        (root))
#define PUSH_STRING_TOKEN(kind_, state_) \
    do {                                          \
        if((state_)->no_copy) {                    \
            PUSH_TOKEN(                        \
                (kind_),                             \
                (state_)->cur_string_start - sizeof string_size_type, \
                (state_));               \
                (state_)->tokens.stack[(state_)->tokens.max - 1].length \
                = (state_)->cur_string_len; \
        } else {                                             \
            PUSH_TOKEN(                          \
                (kind_),                             \
                (state_)->strings.pool + (state_)->strings.cursor, \
                (state_)                             \
            ); \
        } \
    }while(0)

#define INSERT_STRING_TOKEN(kind_, state_, root) \
    INSERT_TOKEN(                                \
        (kind_),                                 \
        (state_)->strings.pool + (state_)->strings.cursor, \
        (state_),                                \
        (root)                                   \
    )

#define START_AND_PUSH_TOKEN(state, kind, string) \
    do {                                          \
        START_STRING(state, string);               \
        PUSH_STRING((state), string, (cs_strlen ((char*)(string)))); \
        PUSH_STRING_TOKEN(kind, state);       \
    }while(0)

#define START_AND_INSERT_TOKEN(state, kind, string, root) \
    START_STRING(state, string);               \
    PUSH_STRING((state), (string), (cs_strlen ((char*)(string)))); \
    INSERT_STRING_TOKEN((kind), (state), (root))
/* __/ */


#undef c99
#endif //JSON_JSON_H
